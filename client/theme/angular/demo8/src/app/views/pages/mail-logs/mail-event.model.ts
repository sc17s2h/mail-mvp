export class MailEventModel {
    id: number;
    email: string;
    event: string;
    timestamp: number;
    category: string;
    'sg_event_id': string;
    'sg_message_id': string;
    'smtp-id': string;
    reason: string;
    status: string;
    response: string;
    attempt: string;
    type: string;
    useragent: string;
    ip: string;
    url: string;
    'asm_group_id': string;
    'send_at': string;
    'sg_content_type': string;
    tls: string;
    'url_offset': string;
}
