// Angular
import { Component, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTable, MatTableDataSource } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { MailEventModel } from './mail-event.model';
import { MailLogsService } from './mail-logs.service';

@Component({
    selector: 'kt-mail-logs',
    templateUrl: './mail-logs.component.html',
    styleUrls: ['mail-logs.component.scss'],
})

export class MailLogsComponent {
    events: MailEventModel[];
    dataSource = new MatTableDataSource<MailEventModel>();

    constructor(private mailLogsService: MailLogsService) { }

    columnsToDisplay = ['sg_event_id', 'sg_message_id', 'email', 'event', 'timestamp'];
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatSort, { static: true }) sort: MatSort;
    selection = new SelectionModel<MailEventModel>(true, []);

    ngOnInit() {
        this.mailLogsService.getAllItems().subscribe(
            (data: MailEventModel[]) => {
                this.dataSource.data = data;
                console.log(data);
                console.log(this.dataSource);
            });
    }
}
