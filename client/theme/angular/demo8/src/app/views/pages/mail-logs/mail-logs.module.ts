// Angular
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
// Core Module
import { CoreModule } from '../../../core/core.module';
import { PartialsModule } from '../../partials/partials.module';
import { MailLogsComponent } from './mail-logs.component';
import { MailLogsService } from './mail-logs.service';

@NgModule({
    imports: [
        CommonModule,
        PartialsModule,
        CoreModule,
        RouterModule.forChild([
            {
                path: '',
                component: MailLogsComponent
            },
        ]),
    ],
    providers: [MailLogsService],
    declarations: [
        MailLogsComponent,
    ]
})
export class MailLogsModule {
}
