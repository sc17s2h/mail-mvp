// Angular
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
// RxJS
import { Observable } from 'rxjs';
// Models
import { MailEventModel } from './mail-event.model';
import { UnixToDatePipe } from '../../../core/_base/layout/pipes/unix-to-date.pipe';

const API_DATATABLE_URL = 'http://localhost:8080/v1/api/events/all';

@Injectable()
export class MailLogsService {
	/**
	 * Service Constructor
	 *
	 * @param http: HttpClient
	 */
    constructor(private http: HttpClient) { }

	/**
	 * Returns data from fake server
	 */
    getAllItems(): Observable<MailEventModel[]> {
        return this.http.get<MailEventModel[]>(API_DATATABLE_URL);
    }
}
