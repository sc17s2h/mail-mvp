import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment'; // add this 1 of 4

/*
 * Raise the value exponentially
 * Takes an exponent argument that defaults to 1.
 * Usage:
 *   value | exponentialStrength:exponent
 * Example:
 *   {{ 2 | exponentialStrength:10 }}
 *   formats to: 1024
*/
@Pipe({name: 'unixToDate'})
export class UnixToDatePipe implements PipeTransform {
  transform(value: number): string {
    return moment.unix(value).format("MM/DD/YY HH:mm");
  }
}