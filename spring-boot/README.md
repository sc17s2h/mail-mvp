
# Elektros Mail (MVP
Email service using Java 8, Spring Boot, and Maven, with MongoDB used to store status logs. All mail is sent using the SendGrid API.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites
```
Java 8
Maven
PostgreSQL
AWS CLI
```
### First time installation
Install AWS CLI
```
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
sudo ./aws/install
```
See https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2.html for more detail.

Configure AWS CLI
```
$ aws configure
AWS Access Key ID [None]: EXAMPLE
AWS Secret Access Key [None]: EXAMPLEKEY
Default region name [None]: us-west-2
Default output format [None]: json
```
See https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-configure.html
for more detail.

Install PostgreSQL

See https://www.postgresql.org/download/


- Don't forget to update application.yml with your AWS keys
- MongoDB only accepts requests from whitelisted IP's so ensure your IP is on the list.


### Run 
Navigate to root directory 
```
cd elektros-mail-mvp
```
Compile maven code
```
mvn compile
```
run
```
mvn spring-boot:run
```
The application is now listening for messages sent to the queue and will process them automatically, and is also listening for SendGrid API events on localhost:8080.

## Deployment

Will update.

## Built With

* [Java 8](https://docs.oracle.com/javase/8/docs/api/) 
* [Maven](https://maven.apache.org/) - Dependency Management
* [SendGrid API](https://sendgrid.com/docs/API_Reference/index.html) - Used to send emails, responses are stores in MongoDB
* [MongoDB](https://docs.spring.io/spring-data/mongodb/docs/current/reference/html/#reference) - Used for storing email status/event logs
* [PostgreSQL](https://www.postgresql.org/download/) - Used for Elektros Database
* [AWS CLI]() - 

## Authors

* **Sam Hepburn** - *Initial work* 