package com.example.demo.entities;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * Base entity for customer data. Most emails will only require this information.
 */
@Data
@Getter
@Setter
public class BaseTemplateData {

    private String customerRef;

    private String customerName;

    private String customerEmail;

    private String companyName;

    public BaseTemplateData() {
    }

    public BaseTemplateData(String customerRef, String customerName, String customerEmail, String companyName) {
        this.companyName = companyName;
        this.customerRef = customerRef;
        this.customerEmail = customerEmail;
        this.customerName = customerName;
    }
}
