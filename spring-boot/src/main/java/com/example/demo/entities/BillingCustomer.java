package com.example.demo.entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;



// TODO: this entity needs to extend BaseTemplateData

/**
 * Entity for billing customers table in Elektros DB
 */
@Entity
@Table(name = "billing_customers")
@Getter
@Setter
public class BillingCustomer {

    @Id
    @Column(columnDefinition = "Serial")
    private Long bcId;

    private String primaryTitle;

    private String primaryFirstname;

    private String primaryLastname;

    @Column(name = "primary_dob", columnDefinition = "DATE")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private Date primaryDob;

    private String phone1;

    private Date createdOn;

    private String email;

    private String customerReferenceNumber;


    public BillingCustomer() {
    }

    @Override
    public String toString() {
        return "\nBillingCustomer{" +
                "bcId=" + bcId +
                ", primaryTitle='" + primaryTitle + '\'' +
                ", primaryFirstname='" + primaryFirstname + '\'' +
                ", primaryLastname='" + primaryLastname + '\'' +
                ", primaryDob=" + primaryDob +
                ", phone1='" + phone1 + '\'' +
                ", createdOn=" + createdOn +
                ", email='" + email + '\'' +
                ", customerReferenceNumber='" + customerReferenceNumber + '\'' +
                "}\n";
    }
}
