package com.example.demo.entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;

// TODO: this entity needs to extend BaseTemplateData

/**
 * Entity for CRM Orders table in Elektros DB
 */
@Entity
@Table(name = "crm_orders")
@Getter
@Setter
public class CrmOrder {

    @Id
    private Long orderId;

    private String cFirstname;

    private String cLastname;

    private String cEmailAddress;

    private String cPhone;

    @Column(name = "c_dob", columnDefinition = "DATE")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "Europe/London")
    private Date cDob;

    private String cDisability;

    private String sFirstname;

    private String sLastname;

    private String sEmailAddress;

    private String sPhone;

    @Column(name = "s_dob", columnDefinition = "DATE")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "Europe/London")
    private Date sDob;

    private String sDisability;

    private Integer supplyBuildingNo;

    private String supplyBuildingName;

    private String supplySubBuilding;

    private String supplyThoroughfare;

    private String supplyDthoroughfare;

    private String supplyTown;

    private String supplyPostcode;

    private Integer billingBuildingNo;

    private String billingBuildingName;

    private String billingSubBuilding;

    private String billingThoroughfare;

    private String billingDthoroughfare;

    private String billingTown;

    private String billingPostcode;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdOn;

    private String orderStatus;

    private Date processedOn;

    private String processedBy;

    private String gasMeterType;

    private String electricityMeterType;

    private String cTitle;

    private String sTitle;

    private String cancelStatus;

    private Integer isNewTenant;

    private Integer isNewConnection;

    @Column(name = "move_in_date", columnDefinition = "DATE")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "Europe/London")
    private Date moveInDate;

    private Long oldCustId;

    private Long custId;

    private String directDebitRef;

    private String sourceId;

    private String comment;

    private BigDecimal estimatedMonthlyCharge;

    private BigDecimal estimatedPowerMonthlyCharge;

    private BigDecimal estimatedGasMonthlyCharge;

    private Integer isMultisite;

    private Integer parentId;

    private String mprn;

    private String gasMeterNumber;

    private String mpan;

    private String powerMeterNumber;

    private Integer isOnline;

    private Integer handleManually;

    private Integer udprn;

    private String bsbId;

    private Integer isCharityOrExempt;

    private String customerReferenceNumber;

    @Column(name = "contract_start_date", columnDefinition = "DATE")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Date contractStartDate;

    @Override
    public String toString() {
        return "CrmOrder{" +
                "orderId=" + orderId +
                ", cFirstname='" + cFirstname + '\'' +
                ", cLastname='" + cLastname + '\'' +
                ", cEmailAddress='" + cEmailAddress + '\'' +
                ", cPhone='" + cPhone + '\'' +
                ", cDob=" + cDob +
                ", cDisability='" + cDisability + '\'' +
                ", sFirstname='" + sFirstname + '\'' +
                ", sLastname='" + sLastname + '\'' +
                ", sEmailAddress='" + sEmailAddress + '\'' +
                ", sPhone='" + sPhone + '\'' +
                ", sDob=" + sDob +
                ", sDisability='" + sDisability + '\'' +
                ", supplyBuildingNo=" + supplyBuildingNo +
                ", supplyBuildingName='" + supplyBuildingName + '\'' +
                ", supplySubBuilding='" + supplySubBuilding + '\'' +
                ", supplyThoroughfare='" + supplyThoroughfare + '\'' +
                ", supplyDthoroughfare='" + supplyDthoroughfare + '\'' +
                ", supplyTown='" + supplyTown + '\'' +
                ", supplyPostcode='" + supplyPostcode + '\'' +
                ", billingBuildingNo=" + billingBuildingNo +
                ", billingBuildingName='" + billingBuildingName + '\'' +
                ", billingSubBuilding='" + billingSubBuilding + '\'' +
                ", billingThoroughfare='" + billingThoroughfare + '\'' +
                ", billingDthoroughfare='" + billingDthoroughfare + '\'' +
                ", billingTown='" + billingTown + '\'' +
                ", billingPostcode='" + billingPostcode + '\'' +
                ", createdOn=" + createdOn +
                ", orderStatus='" + orderStatus + '\'' +
                ", processedOn=" + processedOn +
                ", processedBy='" + processedBy + '\'' +
                ", gasMeterType='" + gasMeterType + '\'' +
                ", electricityMeterType='" + electricityMeterType + '\'' +
                ", cTitle='" + cTitle + '\'' +
                ", sTitle='" + sTitle + '\'' +
                ", cancelStatus='" + cancelStatus + '\'' +
                ", isNewTenant=" + isNewTenant +
                ", isNewConnection=" + isNewConnection +
                ", moveInDate=" + moveInDate +
                ", oldCustId=" + oldCustId +
                ", custId=" + custId +
                ", directDebitRef='" + directDebitRef + '\'' +
                ", sourceId='" + sourceId + '\'' +
                ", comment='" + comment + '\'' +
                ", estimatedMonthlyCharge=" + estimatedMonthlyCharge +
                ", estimatedPowerMonthlyCharge=" + estimatedPowerMonthlyCharge +
                ", estimatedGasMonthlyCharge=" + estimatedGasMonthlyCharge +
                ", isMultisite=" + isMultisite +
                ", parentId=" + parentId +
                ", mprn='" + mprn + '\'' +
                ", gasMeterNumber='" + gasMeterNumber + '\'' +
                ", mpan='" + mpan + '\'' +
                ", powerMeterNumber='" + powerMeterNumber + '\'' +
                ", isOnline=" + isOnline +
                ", handleManually=" + handleManually +
                ", udprn=" + udprn +
                ", bsbId='" + bsbId + '\'' +
                ", isCharityOrExempt=" + isCharityOrExempt +
                ", customerReferenceNumber='" + customerReferenceNumber + '\'' +
                ", contractStartDate=" + contractStartDate +
                '}';
    }
}
