package com.example.demo.entities;

import lombok.Data;

/**
 * Entity for message objects received from the SQS queue.
 */
@Data
public class EmailMessage {

    private String customerRef;

    private String messageType;

    @Override
    public String toString() {
        return "EmailMessage{" +
                "customerRef='" + customerRef + '\'' +
                ", messageType='" + messageType + '\'' +
                '}';
    }
}