package com.example.demo.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Entity for events received from the SendGrid webhooks
 */
@Document
@Getter
@Setter
public class SendGridEvent {

    private String email;

    private String timestamp;

    private String event;

    private String category;

    @JsonProperty("sg_event_id")
    private String sgEventId;

    @JsonProperty("sg_message_id")
    private String sgMessageId;

    @JsonProperty("smtp-id")
    private String smtpId;

//    Delivery

    private String reason;

    private String status;

    private String response;

    private String attempt;

    private String type;


    //    Engagement
    @JsonProperty("useragent")
    private String userAgent;

    private String ip;

    private String url;

    @JsonProperty("asm_group_id")
    private String asmGroupId;


    //    unknown
    @JsonProperty("send_at")
    private String sendAt;

    @JsonProperty("sg_content_type")
    private String sgContentType;

    private String tls;

    @JsonProperty("url_offset")
    private Object urlOffset;

    @Override
    public String toString() {
        return "SendGridEvent{" +
                "email='" + email + '\'' +
                ", timestamp='" + timestamp + '\'' +
                ", event='" + event + '\'' +
                ", category='" + category + '\'' +
                ", sgEventId='" + sgEventId + '\'' +
                ", sgMessageId='" + sgMessageId + '\'' +
                ", smtpId='" + smtpId + '\'' +
                ", reason='" + reason + '\'' +
                ", status='" + status + '\'' +
                ", response='" + response + '\'' +
                ", attempt='" + attempt + '\'' +
                ", type='" + type + '\'' +
                ", userAgent='" + userAgent + '\'' +
                ", ip='" + ip + '\'' +
                ", url='" + url + '\'' +
                ", asmGroupId='" + asmGroupId + '\'' +
                ", sendAt='" + sendAt + '\'' +
                ", sgContentType='" + sgContentType + '\'' +
                ", tls='" + tls + '\'' +
                ", urlOffset=" + urlOffset +
                '}';
    }
}