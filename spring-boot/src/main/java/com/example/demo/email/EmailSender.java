package com.example.demo.email;

import com.example.demo.config.ConfigProperties;
import com.example.demo.enums.TemplateType;
import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGridAPI;
import com.sendgrid.helpers.mail.Mail;
import com.sendgrid.helpers.mail.objects.Content;
import com.sendgrid.helpers.mail.objects.Email;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.StringWriter;

/**
 * Service for the sending of emails using the SendGrid API.
 */
@Service
public class EmailSender {

    @Autowired
    private ConfigProperties configProperties;

    @Autowired
    private SendGridAPI sendGridAPI;

    /**
     * Initialise the velocity engine object with default properties
     *
     * @return VelocityEngine object storing velocity properties
     */
    private VelocityEngine velocityInit() {
        VelocityEngine velocityEngine = new VelocityEngine();
        velocityEngine.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
        velocityEngine.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
        velocityEngine.init();
        return velocityEngine;
    }

    /**
     * Creates and populates relevant Velocity template with customer data, and sends email using SendGrid API.
     *
     * @param from         Email address that the email is to be sent from
     * @param to           Email address that the email is to be sent to
     * @param templateType Email template enum
     * @param data         Customer data to populate the template
     */
    public void sendMail(String from, String to, TemplateType templateType, Object data) {
        String TEMPLATE_URL = configProperties.getTemplateUrl();
        String OBJECT_NAME = configProperties.getObjectName();

//      TODO: sent from name is hard coded to a test email. Uncomment on completion.
        Email sender = new Email("from@example.com", "Yorkshire Energy");
//        Email sender = new Email(from, "Yorkshire Energy");

//      TODO: this email is hard coded to me. Uncomment on completion.
        Email recipient = new Email("sam.hepburn@yorkshire-energy.com");
//        Email recipient = new Email(to);

        VelocityEngine velocityEngine = velocityInit();
        VelocityContext model = new VelocityContext();
        model.put(OBJECT_NAME, data);
        StringWriter stringWriter = new StringWriter();
        velocityEngine.mergeTemplate(TEMPLATE_URL + templateType.getTemplateName(), "UTF-8", model, stringWriter);
        Content content = new Content("text/html", stringWriter.toString());

        Mail mail = new Mail(sender, templateType.getSubject(), recipient, content);

        Request request = new Request();
        try {
            request.setMethod(Method.POST);
            request.setEndpoint("mail/send");
            request.setBody(mail.build());
            Response response = sendGridAPI.api(request);
        } catch (IOException ex) {
//          TODO: Handle exceptions
            ex.printStackTrace();
        }
    }
}
