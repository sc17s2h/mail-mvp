package com.example.demo.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 *
 */
@Configuration
@EnableConfigurationProperties
@ConfigurationProperties()
@Getter
public class ConfigProperties {

    /**
     * AWS properties
     * see https://cloud.spring.io/spring-cloud-aws/reference/html/
     */
    @Value("${cloud.aws.region.static}")
    private String region;
    @Value("${cloud.aws.credentials.access-key}")
    private String awsAccessKey;
    @Value("${cloud.aws.credentials.secret-key}")
    private String awsSecretKey;
    /**
     * SendGrid properties
     */
    @Value("${spring.sendgrid.api-key}")
    private String sendgridKey;
    /**
     * Email Properties
     */
    @Value("${email.templates.url}")
    private String templateUrl;
    @Value("${email.object-name}")
    private String objectName;
    @Value("${email.queue}")
    private String queue;


    public String getTemplateUrl() {
        return templateUrl;
    }


}
