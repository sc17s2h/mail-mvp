package com.example.demo.repositories;

import com.example.demo.entities.SendGridEvent;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * Repository for SendGridEvent objects.
 */
public interface MongoSendGridEventRepository extends MongoRepository<SendGridEvent, String> {

    /**
     * Gets all events stored in MongoDB using MongoRepository
     *
     * @return List of all SendGridEvent objects
     */
    List<SendGridEvent> findAll();

    /**
     * Gets a single event stored in MongoDB by a given SendGrid event identifier using MongoRepository
     *
     * @param sgEventId SendGrid event identifier of the requested event
     * @return SendGridEvent object of the requested event
     */
    SendGridEvent findBySgEventId(String sgEventId);
}

