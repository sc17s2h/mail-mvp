package com.example.demo.repositories;

import com.example.demo.entities.BillingCustomer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Repository for BillingCustomer objects.
 */
public interface BillingCustomerRepository extends JpaRepository<BillingCustomer, Long> {

    /**
     * @param bcId Billing customer identifier for relevant customer
     * @return Billing customer object for relevant customer
     */
    List<BillingCustomer> findByBcId(final Long bcId);

}

