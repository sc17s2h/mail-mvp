package com.example.demo.repositories;

import com.example.demo.entities.CrmOrder;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Repository for CrmOrder objects.
 */
public interface CrmOrderRepository extends JpaRepository<CrmOrder, Long> {

    /**
     * Finds CRM order by value in "order_id" column
     *
     * @param orderId Order identifier of relevant order
     * @return CRM order object of relevant order
     */
    List<CrmOrder> findByOrderId(final Long orderId);

    /**
     * Finds CRM order by value in "customer_reference_number" column
     *
     * @param customerReferenceNumber Customer reference number of relevant order
     * @return CRM order object of relevant order
     */
    CrmOrder findByCustomerReferenceNumber(final String customerReferenceNumber);

}
