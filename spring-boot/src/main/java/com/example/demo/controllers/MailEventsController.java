package com.example.demo.controllers;

import com.example.demo.entities.EmailMessage;
import com.example.demo.entities.SendGridEvent;
import com.example.demo.services.interfaces.MailEventsService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Rest controller for consuming events received from the SendGrid webhook.
 * <p>
 * see https://sendgrid.com/docs/for-developers/tracking-events/event/ for detail of the webhook and example payloads
 */
@RestController
@RequestMapping("/v1/api")
public class MailEventsController {
    private static final Logger LOG = LoggerFactory.getLogger(MailEventsController.class);

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    MailEventsService mailEventsService;

//    TODO: Update javadoc when you know what the exception is

    /**
     * REST endpoint for SendGrid to post event payloads to.
     *
     * @param events Array of JSON objects received from SendGrid webhook, representing email events.
     * @throws JsonProcessingException Not sure yet.
     */
    @PostMapping("/events/new")
    public void addEvent(@RequestBody JsonNode events) throws JsonProcessingException {
        mailEventsService.addEvent(events);
    }

    /**
     * Gets all events stored in MongoDB
     *
     * @return List of all SendGridEvent objects
     */
    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/events/all")
    public List<SendGridEvent> getAllEvents() {
        List<SendGridEvent> x = mailEventsService.getAllEvents();
        for (Object r : x) {
            LOG.info(r.toString());
        }
        return x;
    }

    /**
     * Gets a single event stored in MongoDB by a given SendGrid event identifier
     *
     * @param sgEventId SendGrid event identifier of the requested event
     * @return SendGridEvent object of the requested event
     */
    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/events")
    public SendGridEvent getSingleEvent(@RequestParam(defaultValue = "") String sgEventId) {
        LOG.info(sgEventId);
        SendGridEvent x = null;
        try {
            x = mailEventsService.getSingleEvent(sgEventId);
            LOG.info(x.toString());
        } catch (NullPointerException e) {
            LOG.info("No entry found");
        }
        return x;
    }
}
