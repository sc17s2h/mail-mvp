package com.example.demo.controllers;

import com.example.demo.config.ConfigProperties;
import com.example.demo.entities.EmailMessage;
import com.example.demo.services.implementations.ConsumerServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.aws.messaging.listener.SqsMessageDeletionPolicy;
import org.springframework.cloud.aws.messaging.listener.annotation.SqsListener;
import org.springframework.web.bind.annotation.RestController;

/**
 * Rest controller for consuming messages from SQS queue.
 */
@RestController
public class ConsumerController {

    private static final Logger LOG = LoggerFactory.getLogger(ConsumerController.class);

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private ConsumerServiceImpl consumerServiceImpl;

    @Autowired
    private ConfigProperties configProperties;

    /**
     * Listens for any messages added to the SQS queue, maps them to an EmailMessage object and processes them.
     *
     * @param emailMessage JSON string received from SQS queue.
     */
//    TODO: parameterise the value in application.yml - currently unsure how to access these values in this annotation
    @SqsListener(value = "TestQueue.fifo", deletionPolicy = SqsMessageDeletionPolicy.ON_SUCCESS)
    private void listen(String emailMessage) {
        try {
            EmailMessage em = mapper.readValue(emailMessage, EmailMessage.class);
            consumerServiceImpl.processEmail(em);
//        TODO: Handle exceptions
        } catch (JsonProcessingException e) {
            LOG.error("Input not of valid JSON string form");
        } catch (NullPointerException f) {
            LOG.error("JSON String input contains invalid key values for message");
        }
    }
}

