package com.example.demo.services.interfaces;

import org.springframework.stereotype.Service;

/**
 * Service for SendGrid email methods.
 */
@Service
public interface SendGridEmailService {

    /**
     * Fetches  customer data for relevant customer reference number,
     * and triggers email sender to send the email.
     *
     * @param customerRef Customer reference number for the email recipient
     */
    void sendGoneLiveEmail(String customerRef);

}
