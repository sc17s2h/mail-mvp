package com.example.demo.services.implementations;

import com.example.demo.entities.SendGridEvent;
import com.example.demo.repositories.MongoSendGridEventRepository;
import com.example.demo.services.interfaces.MailEventsService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MailEventsServiceImpl implements MailEventsService {

    private static final Logger LOG = LoggerFactory.getLogger(MailEventsService.class);

    @Autowired
    private MongoSendGridEventRepository mongoSendGridEventRepository;

//    TODO: check why/how this throws JsonProcessingException
//    TODO: try/catch in for loop. Check what exceptions will be thrown by this.
    @Override
    public void addEvent(JsonNode events) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        for (JsonNode root : events) {
            String json = mapper.writeValueAsString(root);
            SendGridEvent sendGridEvent = mapper.readValue(json, SendGridEvent.class);
            LOG.info("Saving " + sendGridEvent.getEvent() + " event to collection...");
            mongoSendGridEventRepository.save(sendGridEvent);
        }
    }

    @Override
    public List<SendGridEvent> getAllEvents() {
        return mongoSendGridEventRepository.findAll();
    }

    @Override
    public SendGridEvent getSingleEvent(String sgEventId) {
        return mongoSendGridEventRepository.findBySgEventId(sgEventId);
    }

}
