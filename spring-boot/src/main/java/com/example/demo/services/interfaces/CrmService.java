package com.example.demo.services.interfaces;

import com.example.demo.entities.BaseTemplateData;
import org.springframework.stereotype.Service;

/**
 * Service for all CrmOrder methods.
 */
@Service
public interface CrmService {

    /**
     * Fetches the base template data for the relevant customer from the crm orders table in Elektros DB.
     *
     * @param customerRef Customer reference number of the recipient.
     * @return BaseTemplateData object with the relevant data for the email recipient.
     */
    BaseTemplateData getBaseTemplateDataFromCrmOrder(String customerRef);

}
