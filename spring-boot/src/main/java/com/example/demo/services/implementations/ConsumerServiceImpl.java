package com.example.demo.services.implementations;

import com.example.demo.entities.EmailMessage;
import com.example.demo.services.interfaces.ConsumerService;
import com.example.demo.services.interfaces.SendGridEmailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ConsumerServiceImpl implements ConsumerService {

    private static final Logger LOG = LoggerFactory.getLogger(ConsumerService.class);

    @Autowired
    private SendGridEmailService sendGridEmailService;

    public void processEmail(EmailMessage em) throws NullPointerException {
        String custRef = em.getCustomerRef();
        String messageType = em.getMessageType();

//        If no messageType or custRef is provided then throw new exception.
//        This is caught in ConsumerController.listen()
        if (custRef == null | messageType == null) {
            throw new NullPointerException();
        }

        LOG.info("Message from SQS Queue - " + em.toString());

        switch (messageType) {
            case "quoteEmail":
                LOG.info("Processing quote email for customer ref: " + custRef);
                break;
            case "goneLive":
                LOG.info("Processing gone live email for customer ref: " + custRef);
                sendGridEmailService.sendGoneLiveEmail(custRef);
                break;
            default:
                LOG.error("Unknown email type");
        }
    }
}
