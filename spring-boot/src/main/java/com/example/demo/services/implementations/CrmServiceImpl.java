package com.example.demo.services.implementations;

import com.example.demo.entities.BaseTemplateData;
import com.example.demo.entities.CrmOrder;
import com.example.demo.repositories.CrmOrderRepository;
import com.example.demo.services.interfaces.CrmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CrmServiceImpl implements CrmService {

    @Autowired
    private final CrmOrderRepository crmOrderRepository;

    public CrmServiceImpl(CrmOrderRepository crmOrderRepository) {
        this.crmOrderRepository = crmOrderRepository;
    }

    public BaseTemplateData getBaseTemplateDataFromCrmOrder(String customerRef) {
        CrmOrder crmOrder = crmOrderRepository.findByCustomerReferenceNumber(customerRef);
        if (crmOrder == null) {
            return null;
        }
        return new BaseTemplateData(
                customerRef,
                crmOrder.getCFirstname(),
                crmOrder.getCEmailAddress(),
                crmOrder.getCLastname()
        );
    }
}
