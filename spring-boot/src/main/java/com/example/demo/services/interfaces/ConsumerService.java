package com.example.demo.services.interfaces;

import com.example.demo.entities.EmailMessage;

/**
 * Service for consumer methods.
 */
public interface ConsumerService {

    /**
     * Checks the data is not null and calls relevant method to send the email based on the customer
     * reference number and the email type.
     *
     * @param em The JSON string message received from the SQS queue.
     */
    void processEmail(EmailMessage em);

}
