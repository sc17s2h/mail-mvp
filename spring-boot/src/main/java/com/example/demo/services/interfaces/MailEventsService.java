package com.example.demo.services.interfaces;

import com.example.demo.entities.SendGridEvent;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;

import java.util.List;

/**
 * Service for mail events methods
 */
public interface MailEventsService {

//    TODO: Update this with the reason for the exception once you've worked it out

    /**
     * Iterates through all new events, maps them to a SendGridEvent object
     * and saves them to the MongoDB collection.
     *
     * @param events Array of JSON objects passed as JsonNode, representing the events received from SendGrid.
     * @throws JsonProcessingException Not sure yet.
     */
    void addEvent(JsonNode events) throws JsonProcessingException;

    /**
     * Gets all events stored in MongoDB using MongoSendGridEventRepository
     *
     * @return List of all SendGridEvent objects
     */
    List<SendGridEvent> getAllEvents();

    /**
     * Gets a single event stored in MongoDB by a given SendGrid event identifier using MongoSendGridEventRepository
     *
     * @param sgEventId SendGrid event identifier of the requested event
     * @return SendGridEvent object of the requested event
     */
    SendGridEvent getSingleEvent(String sgEventId);

}
