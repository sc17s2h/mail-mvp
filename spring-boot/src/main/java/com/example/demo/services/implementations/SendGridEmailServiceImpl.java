package com.example.demo.services.implementations;

import com.example.demo.email.EmailSender;
import com.example.demo.entities.BaseTemplateData;
import com.example.demo.services.interfaces.CrmService;
import com.example.demo.services.interfaces.SendGridEmailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static com.example.demo.enums.TemplateType.GoLiveTemplate;

@Service
public class SendGridEmailServiceImpl implements SendGridEmailService {
    private static final Logger LOG = LoggerFactory.getLogger(SendGridEmailService.class);

    BaseTemplateData baseData = new BaseTemplateData();
    @Autowired
    private EmailSender emailSender;

    @Autowired
    private CrmService crmService;

    @Override
    public void sendGoneLiveEmail(String customerRef) {
        baseData = crmService.getBaseTemplateDataFromCrmOrder(customerRef);

//        Do not try and send if there is no data
        if (baseData != null) {
            String recipient = baseData.getCustomerEmail();
            emailSender.sendMail("from@example.com", recipient, GoLiveTemplate, baseData);
        } else {
//            TODO: handle exception for no entry in table for given cust ref
            LOG.error("No such entry in CRM orders table");
        }
    }
}