package com.example.demo.enums;

/**
 * Enum to store all email templates and the email subject to accompany them.
 */
public enum TemplateType {

    GoLiveTemplate("go_live.vm", "[Yorkshire Energy] - You're Live!"),
    QuoteTemplate("quote.vm", "[Yorkshire Energy] - Your Energy Quote");

    private final String fileName;
    private final String subject;

    TemplateType(String templateName, String subject) {
        this.fileName = templateName;
        this.subject = subject;
    }

    public String getTemplateName() {
        return fileName;
    }

    public String getSubject() {
        return subject;
    }
    }
